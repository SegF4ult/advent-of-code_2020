use std::error::Error;
use std::fmt;
use std::str::FromStr;

#[aoc_generator(day2)]
fn generator_day2(input: &str) -> Vec<PasswordLine> {
    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| PasswordLine::from_str(&l).unwrap())
        .collect()
}

#[derive(Debug)]
struct PasswordLine {
    n1: usize,
    n2: usize,
    token: char,
    password: String,
}

impl PasswordLine {
    pub fn new(n1: usize, n2: usize, token: char, password: String) -> Self {
        PasswordLine {
            n1,
            n2,
            token,
            password,
        }
    }

    fn is_count_valid(&self) -> bool {
        let count = self.password.matches(self.token).count();
        (count >= self.n1) && (count <= self.n2)
    }

    fn contains_token_in_position(&self) -> bool {
        (self.password.chars().nth(self.n1 - 1).unwrap() == self.token)
            ^ (self.password.chars().nth(self.n2 - 1).unwrap() == self.token)
    }
}

impl fmt::Display for PasswordLine {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "({}-{} {}: '{}')",
            self.n1, self.n2, self.token, self.password
        )
    }
}

impl FromStr for PasswordLine {
    type Err = Box<dyn Error>;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // Find delimiters
        let dash = s.find('-').expect("Missing dash");
        let space = s.find(' ').expect("Missing space");
        let colon = s.find(':').expect("Missing colon");
        // Extract information
        let n1 = s[..dash].parse::<usize>().expect("Failed to parse n1");
        let n2 = s[(dash + 1)..space]
            .parse::<usize>()
            .expect("Failed to parse n2");
        let token = s[space + 1..colon]
            .parse::<char>()
            .expect("Failed to parse token");
        let password = String::from(&s[colon + 2..]);
        Ok(PasswordLine::new(n1, n2, token, password))
    }
}

#[aoc(day2, part1)]
fn solve_part1(input: &[PasswordLine]) -> usize {
    input
        .iter()
        .filter(|policy| policy.is_count_valid())
        .count()
}

#[aoc(day2, part2)]
fn solve_part2(input: &[PasswordLine]) -> usize {
    input
        .iter()
        .filter(|policy| policy.contains_token_in_position())
        .count()
}
