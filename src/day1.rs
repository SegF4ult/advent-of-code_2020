#[aoc_generator(day1)]
fn generator_day1(input: &str) -> Vec<u64> {
    input.lines().map(|l| l.parse::<u64>().unwrap()).collect()
}

#[aoc(day1, part1)]
fn solve_part1(input: &[u64]) -> u64 {
    for x in input {
        for y in &input[1..] {
            if (x + y) == 2020 {
                return x * y;
            }
        }
    }
    return 0;
}

#[aoc(day1, part2)]
fn solve_part2(input: &[u64]) -> u64 {
    for x in input {
        for y in &input[1..] {
            for z in &input[2..] {
                if (x + y + z) == 2020 {
                    return x * y * z;
                }
            }
        }
    }
    return 0;
}
